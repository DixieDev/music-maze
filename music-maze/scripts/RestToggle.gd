extends ColorRect

const is_rest_toggle = true
var rests_enabled = false
var hovered = false
var original_color
export(Color) var hovered_color
export(Color) var selected_color

func _ready():
	original_color = color

func on_mouse_over():
	hovered = true
	if not rests_enabled:
		color = hovered_color

func on_mouse_out():
	hovered = false
	if not rests_enabled:
		color = original_color

func toggle():
	rests_enabled = not rests_enabled
	if rests_enabled:
		color = selected_color
	elif hovered:
		color = hovered_color
	else:
		color = original_color
