extends ColorRect

var note = 60
var pressed = false
var hovered = false
var original_color = Color.white
var hover_color = Color.gray
var executing = false
var time_remaining = 0.0

func on_execute():
	executing = true
	hovered = false
	color = original_color

func stop():
	executing = false
	hovered = false
	color = original_color

func on_mouse_over():
	if not executing:
		hovered = true
		color = hover_color

func on_mouse_out():
	if not executing:
		hovered = false
		if not pressed:
			color = original_color

func press():
	pressed = true
	color = hover_color

func release():
	pressed = false
	if not hovered:
		color = original_color
