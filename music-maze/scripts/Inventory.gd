extends ColorRect

signal note_type_selected(note_type)
signal rest_toggled(enable_rest_mode)

onready var rest_toggle = $RestToggle
var selected

func _ready():
	assert(get_child_count() > 0 )
	var default = get_child(0)
	selected = default.note_type
	default.select()
	emit_signal("note_type_selected", default.note_type)

func _input(event):
	if event is InputEventMouseButton and event.button_index == 1 and event.pressed:
		if rest_toggle.hovered:
			toggle_rest()
		else:
			for item in get_children():
				if item.hovered and not item.is_rest_toggle:
					select_item(item)
	elif event is InputEventKey and event.pressed:
		var input = event.as_text()
		if input.empty():
			return
		elif input == "R":
			toggle_rest()
		else:
			var note_type = get_note_type_by_input(input)
			for item in get_children():
				if not item.is_rest_toggle and item.note_type == note_type:
					select_item(item)

func select_item(item):
	for other_item in get_children():
		if not other_item.is_rest_toggle:
			other_item.deselect()
	item.select()
	emit_signal("note_type_selected", item.note_type)

func toggle_rest():
	rest_toggle.toggle()
	emit_signal("rest_toggled", rest_toggle.rests_enabled)

func get_note_type_by_input(input):
	match input:
		"Control+1": return "Crotchet"
		"Control+2": return "Minim"
		"Control+3": return "Quaver"
		"Control+4": return "SemiQuaver"
		_: return ""
