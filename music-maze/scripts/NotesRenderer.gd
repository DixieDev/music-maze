extends Control

onready var sheet = get_node("../../../")

const cursor_border = Color(0.0, 0.63, 0.91, 1.0)
const cursor_fill = Color(0.0, 0.63, 0.91, 0.3)
const cursor_size = Vector2(14, 56)
const cursor_fudge = Vector2(-5, -4)
const cursor_flipped_size = Vector2(17, 56)
var playback_slot = -1

func on_stop_button_pressed():
	playback_slot = -1
	update()

func on_playback_complete():
	playback_slot = -1
	update()

func on_slot_played(slot_idx):
	playback_slot = slot_idx
	update()

func should_dash(line) -> bool:
	return line == sheet.max_line

func _draw():
	var staves_y = sheet.staves_node.rect_position.y
	var stave_node = sheet.staves_node.get_child(0)
	var notes_node = stave_node.get_notes()
	
	var slot_idx = 0
	for slot in sheet.slots:
		var stave_idx = floor(slot_idx / sheet.max_slot)
		var local_slot_idx = slot_idx - stave_idx * sheet.max_slot
		
		var y_disp = staves_y + stave_node.rect_position.y + notes_node.rect_position.y
		var slot_y = rect_position.y + y_disp
		
		var x_disp = stave_node.rect_position.x + notes_node.rect_position.x
		var slot_x = rect_position.x + local_slot_idx * sheet.slot_w + x_disp
		
		var slot_has_flips = false
		if slot.is_rest:
			var rest_pos = sheet.rest_positions[slot.note_type]
			var rest_x = slot_x + rest_pos.x
			var rest_y = slot_y + rest_pos.y
			var pos = Vector2(rest_x, rest_y)
			draw_texture(sheet.rest_textures[slot.note_type], pos)
		else:
			var prev_line = -1
			var prev_flipped = false
			for note in slot.notes:
				var note_y = slot_y + note.line * sheet.line_h
				var fudge = sheet.note_fudges[slot.note_type]
				var pos = Vector2(slot_x, note_y) + fudge
	
				var note_type = slot.note_type
				
				# For quavers and semiquavers in close enough proximity, we 
				# don't want to draw all of their little tail things because
				# things get very messy very quickly
				var line_diff = note.line - prev_line
				var check_tail_merging = note_type == "Quaver" or note_type == "SemiQuaver"
				if check_tail_merging and prev_line >= 0 and line_diff <= 5:
					note_type = "Crotchet"
				
				if should_dash(note.line):
					note_type = "Dashed" + note_type
			
				
				var flip = false
				if prev_line >= 0 and line_diff == 1:
					flip = true
				
				var texture: Texture = sheet.note_textures[note_type]
				if flip and not prev_flipped:
					prev_flipped = true
					slot_has_flips = true
					var size = texture.get_size()
					size.x = -size.x
					pos.x -= fudge.x - 1
					var rect = Rect2(pos, size)
					draw_texture_rect(texture, rect, false)
				else:
					prev_flipped = false
					draw_texture(texture, pos)
				
				prev_line = note.line
		
		if slot_idx == playback_slot:
			var pos = Vector2(slot_x, slot_y) + cursor_fudge
			var rect = Rect2(pos, cursor_size)
			if slot_has_flips:
				rect.size = cursor_flipped_size
			draw_rect(rect, cursor_fill, true)
			draw_rect(rect, cursor_border, false)
		
		slot_idx += 1
		if slot_idx % sheet.max_slot == 0:
			if stave_idx+1 < sheet.staves_node.get_child_count():
				stave_node = sheet.staves_node.get_child(stave_idx+1)
				notes_node = stave_node.get_notes()
			else:
				print("Whoopsy! " + str(stave_idx+1))
