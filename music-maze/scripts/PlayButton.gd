extends Button

signal execute
signal play
signal stop

var composer_expanded = false
var executing = false
var mode = "Execute"
# Modes
# - "Execute"
# - "Play"
# - "Stop"

func on_pressed():
	# Play and switch mode to "Stop"
	if mode == "Play":
		set_mode("Stop")
		emit_signal("play")
		
	# Execute and switch mode to "Stop"
	elif mode == "Execute":
		executing = true
		set_mode("Stop")
		emit_signal("execute")
	
	# Stop and switch mode to "Play" or "Execute", depending on composer's state
	elif mode == "Stop":
		executing = false
		
		if composer_expanded:
			set_mode("Play")
		else:
			set_mode("Execute")
		
		emit_signal("stop")

func on_playback_complete():
	# If executing, playback completion does not end execution.
	if not executing:
		if composer_expanded:
			set_mode("Play")
		else:
			set_mode("Execute")

func set_mode(new_mode):
	mode = new_mode
	text = new_mode

func on_composer_expanded():
	composer_expanded = true
	if mode == "Execute":
		set_mode("Play")

func on_composer_minimised():
	composer_expanded = false
	if mode == "Play":
		set_mode("Execute")
