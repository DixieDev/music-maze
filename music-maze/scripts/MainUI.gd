extends CanvasLayer

# This script basically just propagates signals and vars for external nodes

signal note_pressed(midi_note)
signal note_released(midi_note)
signal execute
signal stop
signal playback_complete()
signal qbeat_progressed

onready var playback = $Composer/Sheet/Playback
export(int) var bpm = 90

func _ready():
	layer = 0
	playback.set_bpm(bpm)

func on_note_pressed(midi_note):
	emit_signal("note_pressed", midi_note)

func on_note_released(midi_note):
	emit_signal("note_released", midi_note)

func on_execute_button_pressed():
	emit_signal("execute")

func on_stop_button_pressed():
	emit_signal("stop")

func on_playback_complete():
	emit_signal("playback_complete")

func on_qbeat_progressed():
	emit_signal("qbeat_progressed")
