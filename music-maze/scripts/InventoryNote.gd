extends ColorRect

const is_rest_toggle = false
var hovered = false
var selected = false
var overdrafted = false
var original_color
export(String) var note_type
export(Color) var hovered_color
export(Color) var selected_color
export(Color) var overdrafted_color

func _ready():
	original_color = color

func on_mouse_over():
	hovered = true
	if not selected:
		color = hovered_color

func on_mouse_out():
	hovered = false
	if not selected:
		if overdrafted:
			color = overdrafted_color
		else:
			color = original_color

func select():
	selected = true
	color = selected_color

func deselect():
	selected = false
	if hovered:
		color = hovered_color
	else:
		color = original_color

func set_overdrafted():
	overdrafted = true
	color = overdrafted_color

func clear_overdraft():
	overdrafted = true
	if hovered:
		color = hovered_color
	elif selected:
		color = selected_color
	else:
		color = original_color
