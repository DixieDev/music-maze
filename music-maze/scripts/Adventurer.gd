extends Sprite

export (float) var speed = 140.0
onready var collision_map = get_node("../CollisionMap")
onready var gates_container = get_node("../Gates")
onready var arrow = get_node("Arrow");

var locked = true
var moving = false
var movement_start = Vector2(0, 0)
var initial_position = position
var initial_direction = 0

var direction = 0  # See array below
var directions = [
	Vector2(0.0, -1.0), # 0 - Up
	Vector2(1.0, 0.0),  # 1 - Right
	Vector2(0.0, 1.0),  # 2 - Down
	Vector2(-1.0, 0.0)  # 3 - Left
]
var arrow_rotations = [
	0.0,
	90.0,
	180.0,
	270.0
]

var w = 16
var h = 16
var cell_w = 16
var cell_h = 16
export(int) var qbeats_per_move = 2
var qbeats = 0

var gates = []

# Called when the node enters the scene tree for the first time.
func _ready():
	assert(collision_map != null)
	assert(gates_container != null)
	assert(arrow != null)
	
	gates = gates_container.get_children()
	assert(gates != null and not gates.empty())
	
	w = region_rect.size.x
	h = region_rect.size.y
	cell_w = collision_map.cell_size.x
	cell_h = collision_map.cell_size.y
	
	snap_to_grid()
	initial_position = position
	initial_direction = direction
	qbeats = qbeats_per_move
	arrow.rotation_degrees = arrow_rotations[direction]

func on_execute():
	locked = false

func reset():
	locked = true
	moving = false
	position = initial_position
	direction = initial_direction
	qbeats = qbeats_per_move
	arrow.rotation_degrees = arrow_rotations[direction]

func _process(delta):
	if locked:
		return
	
	if moving:
		var velocity = directions[direction] * speed
		position += velocity * delta
		
		var x_disp = movement_start.x - position.x
		var y_disp = movement_start.y - position.y
			
		if (direction == 1 or direction == 3) and abs(x_disp) >= cell_w:
			moving = false
			if direction == 1:
				position.x = movement_start.x + cell_w
			else:
				position.x = movement_start.x - cell_w
		
		elif (direction == 0 or direction == 2) and abs(y_disp) >= cell_h:
			moving = false
			if direction == 0:
				position.y = movement_start.y - cell_h
			else:
				position.y = movement_start.y + cell_h

func check_upcoming_collision() -> bool:
	var coords = collision_map.world_to_map(position)
	var next_coords = coords + directions[direction]
	var next_wall_cell = collision_map.get_cellv(next_coords)
	if next_wall_cell != TileMap.INVALID_CELL:
		return true
	elif check_gate_collision(next_coords):
		return true
	else:
		return false

func check_gate_collision(coords):
	for gate in gates:
		if gate.visible:
			var gate_coord = collision_map.world_to_map(gate.position)
			if coords == gate_coord:
				return true
	return false

func is_on_gate(gate) -> bool:
	var coords = collision_map.world_to_map(position)
	var gate_coords = collision_map.world_to_map(gate.position)
	if coords == gate_coords:
		return true
	else:
		return false

func turn_once():
	direction = (direction + 1) % 4
	arrow.rotation_degrees = arrow_rotations[direction]

func snap_to_grid():
	var coords = collision_map.world_to_map(position)
	position.x = coords.x * cell_w + w/2
	position.y = coords.y * cell_h + h/2

func on_qbeat():
	qbeats += 1
	if qbeats >= qbeats_per_move:
		qbeats = 0
		if (check_upcoming_collision()):
			turn_once()
		else:
			moving = true
			movement_start = position
