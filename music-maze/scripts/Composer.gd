extends Control

signal expanded
signal minimised

onready var animation_player = $AnimationPlayer

var state = "Minimised"
# States:
# - Disabled
# - Minimised
# - Expanding
# - Expanded
# - Minimising

func _ready():
	rect_position.y = 150

func enable():
	state = "Minimised"

func disable():
	state = "Disabled"
	animation_player.play("Minimise")

func expand():
	state = "Expanding"
	animation_player.play("Expand")

func minimise():
	state = "Minimising"
	animation_player.play("Minimise")

func on_expander_pressed():
	if state == "Minimised":
		expand()
		emit_signal("expanded")
	elif state == "Expanded":
		minimise()
		emit_signal("minimised")

func on_animation_finished(anim_name):
	if anim_name == "Expand":
		state = "Expanded"
	elif anim_name == "Minimise" and state != "Disabled":
		state = "Minimised"
