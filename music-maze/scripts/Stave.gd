extends Control

onready var notes = $Notes

func get_notes() -> Control:
	return notes
