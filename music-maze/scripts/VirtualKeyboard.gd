extends ColorRect

signal key_pressed(midi_note)
signal key_released(midi_note)

onready var control = get_node("../")
onready var whites = $Whites
onready var blacks = $Blacks
onready var midi_player = get_node("../MidiPlayer")

export(int) var num_whites = 12
const note_y = 5
const white_w = 8
const white_h = 34
const white_padding = 2
const black_w = 4
const black_h = 26
const key_script = preload("res://scripts/VkbNote.gd")

const black_sequence = [
	true,
	true,
	false,
	true,
	true,
	true,
	false,
]

var keys: Array = []
var playing = false
var executing = false

const note_on = 0x09
const note_velocity = 0x7F
export(int) var initial_note = 60

func _ready():
	assert(midi_player != null)
	assert(key_script != null)
	create_keys()

func on_execute_button_pressed():
	on_play()
	executing = true

func on_play_button_pressed():
	on_play()

func on_stop_button_pressed():
	stop()
	executing = false

func on_playback_complete():
	stop()

func on_playback_note_pressed(note):
	press_key(note)

func on_playback_note_released(note):
	release_key(note)

func _process(dt):
	for key in keys:
		if key.time_remaining > 0:
			key.time_remaining -= dt
			if key.time_remaining <= 0:
				key.release()
				release_note(key.note)

func _input(event):
	if playing:
		return
	
	if event is InputEventMouseButton and event.button_index == 1:
		if event.pressed == true:
			for key in keys:
				if key.hovered:
					key.time_remaining = 0.0
					key.press()
					play_note(key.note)
		else:
			for key in keys:
				if key.pressed and key.time_remaining <= 0:
					key.release()
					release_note(key.note)

func on_play():
	playing = true
	for key in keys:
		key.on_execute()
		if key.pressed:
			key.release()
			release_note(key.note)

func stop():
	playing = false
	for key in keys:
		key.stop()
		if key.pressed:
			key.release()
			release_note(key.note)

func on_sheet_note_added(note_type, midi_note):
	if not executing and note_type != "Rest":
		press_key(midi_note, 0.16)

# Use play_note internally. This is for executing externally
func press_key(note, fixed_time = 0.0):
	for key in keys:
		if key.note == note:
			key.press()
			key.time_remaining = fixed_time
			play_note(note)
			return
	assert(false) # Provided note does not exist

# Use release_note internally. This is for executing externally
func release_key(note):
	for key in keys:
		if key.note == note:
			key.release()
			release_note(note)
			return
	assert(false) # Provided note does not exist

func play_note(note):
	var input_event: InputEventMIDI = InputEventMIDI.new()
	input_event.message = note_on
	input_event.pitch = note
	input_event.velocity = note_velocity
	midi_player.receive_raw_midi_message(input_event)
	emit_signal("key_pressed", note)

func release_note(note):	
	var input_event: InputEventMIDI = InputEventMIDI.new()
	input_event.message = note_on
	input_event.pitch = note
	input_event.velocity = 0
	midi_player.receive_raw_midi_message(input_event)
	emit_signal("key_released", note)

func create_keys():
	var total_width = (white_w + white_padding) * num_whites - white_padding
	var start_x = (rect_size.x / 2) - total_width / 2
	var x = start_x
	var y = note_y
	var note = initial_note
	
	for i in range(num_whites):
		create_key(whites, Vector2(x, y), Vector2(white_w, white_h), Color.white, Color.gray, note)
		note += 1
		x += white_w
		var black_x = x + white_padding/2 - black_w/2
		x += white_padding
		if i < num_whites - 1 and black_sequence[i % black_sequence.size()]:
			create_key(blacks, Vector2(black_x, y), Vector2(black_w, black_h), Color.black, Color.gray, note)
			note += 1

func create_key(pool, position: Vector2, size: Vector2, color: Color, hover_color: Color, note):
	var key = ColorRect.new()
	key.set_script(key_script)
	key.connect("mouse_entered", key, "on_mouse_over")
	key.connect("mouse_exited", key, "on_mouse_out")
	key.note = note
	key.original_color = color
	key.hover_color = hover_color
	key.rect_position = position
	key.rect_size = size
	key.color = color
	key.visible = true
	keys.append(key)
	pool.add_child(key)

