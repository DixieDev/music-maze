extends Control

signal qbeat_progressed
signal slot_played(slot_idx)
signal playback_complete
signal play_note(note)
signal release_note(note)

onready var sheet = get_node("../")
onready var scroll_container = get_node("../ScrollContainer")

var state = "Inactive"
# States:
# - Inactive
# - Playing
# - Executing

var slots_on_playback = 0
var qbeat_duration = 15 / 90
var accumulated_dt = 0
var next_slot_idx = 0
var remaining_qbeats = 1

func set_bpm(bpm):
	qbeat_duration = 15.0 / bpm

func begin_playback():
	accumulated_dt = 0
	next_slot_idx = 0
	remaining_qbeats = 1
	slots_on_playback = sheet.slots.size()

func stop_playback():
	state = "Inactive"

func on_execute_button_pressed():
	if state == "Inactive":
		state = "Executing"
		begin_playback()

func on_play_button_pressed():
	if state == "Inactive":
		state = "Playing"
		begin_playback()

func on_stop_button_pressed():
	if state != "Inactive":
		stop_playback()

func _process(dt):
	if state != "Inactive":
		accumulated_dt += dt
		
		# This kind of thing is usually done as a while-loop, to make up for 
		# lost time. Doing that here, however, would either mean spamming every
		# missed note, or just skipping them entirely. 
		#
		# Instead I have opted to handle each qbeat after *at least* 
		# qbeat_duration seconds, which is technically incorrect but offers the
		# best balance of functionality to enjoyment, in my opinion!
		var qbeat_progressed = false
		if accumulated_dt >= qbeat_duration:
			accumulated_dt = 0
			remaining_qbeats -= 1
			qbeat_progressed = true
		
		# If there are no remaining qbeats from the previous slot's delay, then
		# attempt to play the next slot
		if remaining_qbeats <= 0:
			if next_slot_idx < slots_on_playback:
				var prev_slot_idx = next_slot_idx - 1
				if prev_slot_idx >= 0:
					var prev_slot = sheet.slots[prev_slot_idx]
					for note in prev_slot.notes:
						var midi_note = sheet.line_to_midi_note(note.line)
						emit_signal("release_note", midi_note)
				
				# Play next slot. There should be no notes if it's a rest, but
				# the remaining_qbeats is still set so there is a delay before
				# the next slot is played
				var slot = sheet.slots[next_slot_idx]
				remaining_qbeats = slot.qbeat_length
				for note in slot.notes:
					var midi_note = sheet.line_to_midi_note(note.line)
					emit_signal("play_note", midi_note)
				
				emit_signal("slot_played", next_slot_idx)
				next_slot_idx += 1
				
			else:
				# In this case, we have reached the end of the available slots,
				# so playback is complete
				if state == "Playing":
					stop_playback()
				emit_signal("playback_complete")
		
		if qbeat_progressed:
			emit_signal("qbeat_progressed")
