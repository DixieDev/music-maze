extends ColorRect

signal note_added(note_type, midi_note)
signal note_removed(note_type)

onready var scroll_container = $ScrollContainer
onready var scrollable_content_node = $ScrollContainer/ScrollableContent
onready var staves_node = $ScrollContainer/ScrollableContent/Staves
onready var cursor_node = $ScrollContainer/ScrollableContent/Cursor
onready var notes_renderer = $ScrollContainer/ScrollableContent/NotesRenderer
onready var stave_scene = preload("res://scenes/Stave.tscn")

const midi_middle_c = 60
const midi_white_steps = [2, 2, 1, 2, 2, 2, 1]
const min_line = 1
const max_line = 12
const min_slot = 0
const max_slot = 24
const line_h = 4
const slot_w = 16
const stave_padding = 8

var state = "Composing"
# States
# - Composing
# - Playing

var slots = [
	# One object for each slot that contains notes
	# {
	# 	"qbeat_placement": int,
	# 	"qbeat_length": int,
	# 	"note_type": "Crotchet"/"Minim"/"Quaver"/"SemiQuaver",
	# 	"is_rest": bool,
	# 	"notes": [
	# 		{
	# 			"line": int,
	# 		}
	# 	],
	# },
]

const note_fudges = {
	"Crotchet": Vector2(-4, -22),
	"Minim": Vector2(-4, -22),
	"Semibreve": Vector2(-4, -22),
	"Quaver": Vector2(-4, -22),
	"SemiQuaver": Vector2(-4, -22),
}

const rest_positions = {
	"Crotchet": Vector2(-4, 10),
	"Minim": Vector2(-4, 2),
	"Semibreve": Vector2(-4, 2),
	"Quaver": Vector2(-4, 14),
	"SemiQuaver": Vector2(-4, 14),
}

const note_qbeats = {
	"Crotchet": 4,
	"Minim": 8,
	"Semibreve": 16,
	"Quaver": 2,
	"SemiQuaver": 1,
}

onready var note_textures = {
	"Crotchet": preload("res://art/crotchet.png"),
	"DashedCrotchet": preload("res://art/crotchet_dashed.png"),
	"Minim": preload("res://art/minim.png"),
	"DashedMinim": preload("res://art/minim_dashed.png"),
	"Semibreve": preload("res://art/minim.png"),
	"DashedSemibreve": preload("res://art/minim_dashed.png"),
	"Quaver": preload("res://art/quaver.png"),
	"DashedQuaver": preload("res://art/quaver_dashed.png"),
	"SemiQuaver": preload("res://art/semiquaver.png"),
	"DashedSemiQuaver": preload("res://art/semiquaver_dashed.png"),
}

onready var rest_textures = {
	"Crotchet": preload("res://art/crotchet_rest.png"),
	"Minim": preload("res://art/minim_rest.png"),
	"Semibreve": preload("res://art/minim_rest.png"),
	"Quaver": preload("res://art/quaver_rest.png"),
	"SemiQuaver": preload("res://art/semiquaver_rest.png"),
}

var equipped_note_type = "Crotchet"
var equipped_rest = false
var valid_hover = false
var hovered_line: int = 0
var hovered_slot_idx: int = 0
var prev_placed_note = {
	"is_valid": false,
	"is_rest": false,
	"slot_idx": 0,
	"line": 0,
}
var held_midi_inputs = 0
var midi_slot_idx = 0

func _ready():
	OS.open_midi_inputs()
	equip_note_type("Quaver")

func set_composing():
	state = "Composing"

func set_playing():
	state = "Playing"

func on_execute_button_pressed():
	set_playing()

func on_play_button_pressed():
	set_playing()

func on_stop_button_pressed():
	set_composing()

func on_playback_complete():
	set_composing()

func on_inventory_note_type_selected(note_type):
	equip_note_type(note_type)

func equip_note_type(note_type):
	equipped_note_type = note_type
	if equipped_rest:
		cursor_node.texture = rest_textures[note_type]
	else:
		cursor_node.texture = note_textures[note_type]

func _input(event):
	if state != "Composing":
		cursor_node.visible = false
		return
	
	if event is InputEventMouseMotion:
		on_mouse_move()
	
	elif event is InputEventMouseButton and event.pressed and valid_hover:
		if event.button_index == 1:
			place_note(equipped_note_type)
			fit_staves()
		elif event.button_index == 2:
			remove_note()
			fit_staves()
	
	elif event is InputEventKey and event.pressed:
		# Keyboard input for inserting and manipulating notes
		on_key_press(event.as_text())
		fit_staves()
	
	elif event is InputEventMIDI:
		var line = midi_note_to_line(event.pitch)
		if line > 0: 
			if event.message == 9:
				on_midi_press(line)
				fit_staves()
			elif event.message == 8: 
				on_midi_release()
				fit_staves()

func on_midi_press(line):
	if held_midi_inputs <= 0:
		held_midi_inputs = 0
		midi_slot_idx = slots.size()
	
	hovered_slot_idx = midi_slot_idx
	hovered_line = line
	place_note(equipped_note_type)
	
	held_midi_inputs += 1

func on_midi_release():
	held_midi_inputs -= 1

func log_midi_event(event):
	print("MIDI event on channel: " + str(event.channel))
	print("	Controller #" + str(event.controller_number))
	print("	Controller Value: " + str(event.controller_value))
	print("	Instrument: " + str(event.instrument))
	print("	Message: " + str(event.message))
	print("	Pitch: " + str(event.pitch))
	print("	Pressure: " + str(event.pressure))
	print("	Velocity: " + str(event.velocity))

func on_key_press(input: String):
	print(input)
	match input:
		"Up": shift_prev_placed_note(-1)
		"Down": shift_prev_placed_note(1)
		"C": place_note_by_keypress(12)
		"D": place_note_by_keypress(11)
		"E": place_note_by_keypress(10)
		"F": place_note_by_keypress(9)
		"G": place_note_by_keypress(8)
		"A": place_note_by_keypress(7)
		"B": place_note_by_keypress(6)
		"1": place_note_relative(1)
		"2": place_note_relative(2)
		"3": place_note_relative(3)
		"4": place_note_relative(4)
		"5": place_note_relative(5)
		"6": place_note_relative(6)
		"7": place_note_relative(7)
		"8": place_note_relative(8)
		"9": place_note_relative(9)
		"Shift+1": place_note_relative(-1)
		"Shift+2": place_note_relative(-2)
		"Shift+3": place_note_relative(-3)
		"Shift+4": place_note_relative(-4)
		"Shift+5": place_note_relative(-5)
		"Shift+6": place_note_relative(-6)
		"Shift+7": place_note_relative(-7)
		"Shift+8": place_note_relative(-8)
		"Shift+9": place_note_relative(-9)
		"Shift+C": place_note_by_keypress(5)
		"Shift+D": place_note_by_keypress(4)
		"Shift+E": place_note_by_keypress(3)
		"Shift+F": place_note_by_keypress(2)
		"Shift+G": place_note_by_keypress(1)
		"BackSpace":
			var slot_idx = slots.size()-1
			if slot_idx >= 0:
				hovered_slot_idx = slot_idx
				if slots[slot_idx].is_rest:
					remove_note()
				else:
					while not slots[slot_idx].notes.empty():
						hovered_line = slots[slot_idx].notes[0].line
						remove_note()

func place_note_by_keypress(line):
	hovered_line = line
	if not valid_hover:
		var slot_idx = slots.size()
		hovered_slot_idx = slot_idx
	place_note(equipped_note_type)

func place_note_relative(offset):
	var target_slot_idx = -1
	var base_line = 0
	
	if valid_hover and hovered_slot_idx < slots.size():
		var slot = slots[hovered_slot_idx]
		# If hovered over a slot with a single note, we're adding a note 
		# relative to that existing note
		if slot.notes.size() == 1:
			base_line = slot.notes[0].line
			target_slot_idx = hovered_slot_idx
		
		# If the slot has multiple notes, we'll add a note relative to whichever
		# note the user is hovered over
		else:
			for note in slot.notes:
				if note.line == hovered_line:
					base_line = hovered_line
					target_slot_idx = hovered_slot_idx
					break
	
	# If the user isn't hovered over a note we can unambiguously place the new
	# note next to, check for the most recently placed note instead
	if base_line <= 0 and prev_placed_note.is_valid and not prev_placed_note.is_rest:
		base_line = prev_placed_note.line
		target_slot_idx = prev_placed_note.slot_idx
	
	var note_type = slots[target_slot_idx].note_type
	
	# If we have a valid base_line, place a note relative to it
	var target_line = base_line + offset
	if base_line > 0 and base_line <= max_line and target_line > 0 and target_line <= max_line:
		hovered_slot_idx = target_slot_idx
		hovered_line = target_line
		place_note(note_type)
	

func shift_prev_placed_note(displacement):
	if not prev_placed_note.is_valid or prev_placed_note.is_rest :
		return
	
	var line = prev_placed_note.line
	var new_line = line + displacement
	var slot = slots[prev_placed_note.slot_idx]
	
	while true:
		var required_shift = false
		for note in slot.notes:
			if note.line == new_line:
				required_shift = true
				new_line += displacement
		if new_line <= 0 or new_line > max_line:
			return
		elif not required_shift:
			break

	hovered_slot_idx = prev_placed_note.slot_idx
	hovered_line = line
	remove_note()
	hovered_line = new_line
	place_note(slot.note_type)

func fit_staves():
	var num_staves = staves_node.get_child_count()
	var required_staves = 1 + ceil(slots.size() / max_slot)
	
	if required_staves > num_staves:
		var stave_idx = num_staves
		while required_staves > staves_node.get_child_count():
			add_stave(stave_idx)
			stave_idx += 1
	else:
		var stave_idx = num_staves - 1
		while required_staves < staves_node.get_child_count():
			remove_stave(stave_idx)
			stave_idx -= 1
	
	var stave = staves_node.get_child(0)
	var staves_height = (stave.rect_size.y + stave_padding) * required_staves
	scrollable_content_node.rect_min_size.y = stave.rect_position.y + staves_height

func add_stave(stave_idx):
	var stave_node = stave_scene.instance()
	stave_node.rect_position.y = (stave_node.rect_size.y + stave_padding) * stave_idx
	staves_node.add_child(stave_node)

func remove_stave(stave_idx):
	staves_node.remove_child(staves_node.get_child(stave_idx))

func on_mouse_move():
	var mouse_pos = staves_node.get_local_mouse_position()
	
	# Check if mouse is within note-placing range of any staves
	valid_hover = false
	var stave_idx = 0
	for stave in staves_node.get_children():
		var rect = Rect2(stave.rect_position, stave.rect_size)
		if rect.has_point(mouse_pos):
			valid_hover = handle_mouse_in_stave(stave_idx, stave)
			break
		stave_idx += 1
	
	cursor_node.visible = valid_hover

func stave_idx_from_global_slot_idx(global_slot_idx):
	return floor(global_slot_idx / max_slot)

func local_slot_idx_from_global(global_slot_idx):
	return global_slot_idx % max_slot

func handle_mouse_in_stave(stave_idx: int, stave_node: Control) -> bool:
	var notes_node: Control = stave_node.get_notes()
	var mouse_pos = notes_node.get_local_mouse_position()
	
	var slot_idx = clamp(round(mouse_pos.x / slot_w), min_slot-1, max_slot+1)
	var global_slot_idx = stave_idx * max_slot + slot_idx
	if slot_idx < min_slot or slot_idx >= max_slot or global_slot_idx > slots.size():
		return false
	
	var line = clamp(ceil(mouse_pos.y / line_h), min_line, max_line)
	
	var cursor_dest = notes_node.rect_global_position
	cursor_dest.x += slot_idx * slot_w
	if equipped_rest:
		cursor_dest += rest_positions[equipped_note_type]
	else:
		cursor_dest.y += line * line_h
		cursor_dest += note_fudges[equipped_note_type]
	var displacement = cursor_dest - cursor_node.rect_global_position
	
	cursor_node.rect_position += displacement
	
	hovered_line = line
	hovered_slot_idx = slot_idx + stave_idx * max_slot
	
	return true

func place_note(note_type):
	var slot_idx = hovered_slot_idx
	var qbeat_length = note_qbeats[note_type]
	
	var needs_fit = false
	if slot_idx < slots.size():
		var slot = slots[slot_idx]
		if slot.qbeat_length != qbeat_length:
			# If there's another note here and the qbeat duration is different, 
			# we're gonna need to do some work to fit this new one in
			needs_fit = true
		elif slot.is_rest and equipped_rest:
			# Don't place rest if an identical one exists
			prev_placed_note.is_valid = true
			prev_placed_note.is_rest = true
			prev_placed_note.slot_idx = slot_idx
			return
		elif not equipped_rest:
			# Don't place note if an identical one exists
			for note in slot.notes:
				if note.line == hovered_line:
					prev_placed_note.is_valid = true
					prev_placed_note.is_rest = false
					prev_placed_note.slot_idx = slot_idx
					prev_placed_note.line = note.line
					return
	
	var qbeat_placement = 0
	if slot_idx > 0:
		var prev_slot = slots[slot_idx-1]
		qbeat_placement = prev_slot.qbeat_placement + prev_slot.qbeat_length
	
	var new_note = {
		"line": hovered_line
	}
	
	var new_notes = []
	if not equipped_rest:
		new_notes = [new_note]
	
	prev_placed_note.is_valid = true
	prev_placed_note.slot_idx = slot_idx
	prev_placed_note.is_rest = equipped_rest
	prev_placed_note.line = new_note.line
	
	if slot_idx < slots.size():
		# Handle adjustments to an existing slot 
		if not needs_fit:
			# Edit the existing slot to change its status as a rest, or to add
			# a note to its note array
			slots[slot_idx].is_rest = equipped_rest
			if equipped_rest:
				slots[slot_idx].notes.clear()
			else:
				slots[slot_idx].notes.append(new_note)
				slots[slot_idx].notes.sort_custom(self, "note_comparison")
		else:
			# Things start getting spicy. We need to add a new slot for the 
			# new note, and then update the old slot to cover the remainder
			# of the duration. This can involve introducing additional new notes
			# such as when a quaver is inserted on top of a minim.
			var new_slot = {
				"qbeat_placement": qbeat_placement,
				"qbeat_length": qbeat_length,
				"note_type": note_type,
				"is_rest": equipped_rest,
				"notes": new_notes
			}
			fit_new_slot(slot_idx, new_slot)
	else:
		# Append a completely new slot
		var new_slot = {
			"qbeat_placement": qbeat_placement,
			"qbeat_length": qbeat_length,
			"note_type": note_type,
			"is_rest": equipped_rest,
			"notes": new_notes
		}
		slots.append(new_slot)
	
	# Align the scrollbar to include the relevant stave
	var stave_idx = floor(slot_idx / max_slot)
	var hovered_stave = staves_node.get_child(stave_idx)
	var stave_min_y = hovered_stave.rect_position.y
	var stave_max_y = stave_min_y + hovered_stave.rect_size.y
	var container_min_y = scroll_container.scroll_vertical
	var container_max_y = container_min_y + scroll_container.rect_size.y
	if container_max_y < stave_max_y:
		scroll_container.scroll_vertical = stave_min_y
	if container_min_y > stave_min_y:
		scroll_container.scroll_vertical = stave_min_y
	
	notes_renderer.update()
	if equipped_rest:
		emit_signal("note_added", "Rest", line_to_midi_note(hovered_line))
	else:
		emit_signal("note_added", note_type, line_to_midi_note(hovered_line))

func note_comparison(a, b) -> bool:
	return a.line < b.line

func fit_new_slot(slot_idx, new_slot):
	var old_slot = slots[slot_idx]
	var qbeat_diff = old_slot.qbeat_length - new_slot.qbeat_length
	if qbeat_diff > 0:
		# If the old slot had a longer duration, we need to reduce its duration,
		# which may involve splitting it into multiple different note types.
		split_slot(slot_idx, new_slot, old_slot, new_slot.qbeat_length)
	elif qbeat_diff < 0:
		# If the old slot has a short duration, we need to remove it, and then
		# possibly remove consecutive notes too until the duration of the new
		# note is covered. This may involve splitting an old slot at the end.
		consume_slots_for_new_slot(slot_idx, new_slot)

func split_slot(slot_idx, new_slot, old_slot, new_slot_qbeat_length):
	slots.insert(slot_idx, new_slot)
	old_slot.qbeat_length -= new_slot_qbeat_length
	old_slot.qbeat_placement += new_slot_qbeat_length
	
	match old_slot.qbeat_length:
		# Some of these are easily solved by just updating the old slot's note 
		# type, while others require new slots to be introduced.
		1: old_slot.note_type = "SemiQuaver"
		2: old_slot.note_type = "Quaver"
		3: split_slot_using_type(slot_idx+1, "SemiQuaver", old_slot)
		4: old_slot.note_type = "Crotchet"
		6: split_slot_using_type(slot_idx+1, "Quaver", old_slot)
		7: split_slot_using_type(slot_idx+1, "SemiQuaver", old_slot)
		8: old_slot.note_type = "Minim"
		12: split_slot_using_type(slot_idx+1, "Crotchet", old_slot)
		14: split_slot_using_type(slot_idx+1, "Quaver", old_slot)
		15: split_slot_using_type(slot_idx+1, "SemiQuaver", old_slot)
		_: print("Oh shit oh fuck")

func split_slot_using_type(slot_idx, new_type, old_slot):
	var new_slot = {
		"qbeat_placement": old_slot.qbeat_placement,
		"qbeat_length": note_qbeats[new_type],
		"note_type": new_type,
		"is_rest": old_slot.is_rest,
		"notes": []
	}
	for note in old_slot.notes:
		new_slot.notes.append(note)
	split_slot(slot_idx, new_slot, old_slot, new_slot.qbeat_length)

func consume_slots_for_new_slot(slot_idx, new_slot):
	var remaining_qbeats = new_slot.qbeat_length
	while remaining_qbeats > 0 and slot_idx < slots.size():
		var old_slot = slots[slot_idx]
		var next_remaining_qbeats = remaining_qbeats - old_slot.qbeat_length
		if next_remaining_qbeats < 0:
			split_slot(slot_idx, new_slot, old_slot, remaining_qbeats)
			return
		
		slots.remove(slot_idx)
		remaining_qbeats = next_remaining_qbeats
	
	slots.insert(slot_idx, new_slot)

func get_note_type_by_qbeats(qbeats) -> String:
	match qbeats:
		1: return "Semi"
		2: return "Quaver"
		4: return "Crotchet"
		8: return "Minim"
		16: return "Semibreve"
		_: return ""

func remove_note():
	var slot_idx = hovered_slot_idx
	
	# If hovered slot isn't filled, exit early
	if slot_idx < 0 or slot_idx >= slots.size():
		return
	
	var slot = slots[slot_idx]
	
	prev_placed_note.is_valid = false
	
	# Handle the case where we're deleting from a slot prior to the final slot
	if slot_idx < slots.size()-1 and slot.notes.size() <= 1:
		# If it's a rest, remove the slot entirely
		if slot.is_rest:
			slots.remove(slot_idx)
			emit_signal("note_removed", slot.note_type)
			
		# Otherwise it's a single note on its own, and should become a rest
		else:
			slot.notes.clear()
			slot.is_rest = true
		
		notes_renderer.update()
	
	# Else, if the slot is a rest, just delete the slot entirely
	elif slot.is_rest:
		slots.remove(slot_idx)
		emit_signal("note_removed", slot.note_type)
	
	# Else if there is only one note, just replace it with a rest
	elif slot.notes.size() == 1:
		slot.notes.clear()
		slot.is_rest = true
	
	# Else, find  and remove hovered note from slot array
	else:
		var note_idx = 0
		for note in slot.notes:
			if note.line == hovered_line:
				# Remove just this note from the slot
				slot.notes.remove(note_idx)
				
				emit_signal("note_removed", slot.note_type)
				break
			note_idx += 1
	
	# Tell the renderer to update draw instructions
	notes_renderer.update()

func create_note(note_type, slot, line) -> TextureRect:
	var note = TextureRect.new()
	var texture = note_textures[note_type]
	note.texture = texture
	note.rect_position.x = slot * slot_w
	note.rect_position.y = line * line_h
	note.rect_position += note_fudges[note_type]
	note.rect_size.x = texture.get_size().x
	note.rect_size.y = texture.get_size().y
	note.visible = true
	note.mouse_filter = Control.MOUSE_FILTER_IGNORE
	return note

func line_to_midi_note(line):
	var diff_from_c = max_line - line 
	var note = midi_middle_c
	for i in diff_from_c:
		note += midi_white_steps[i % midi_white_steps.size()]
	return note

func midi_note_to_line(midi_note):
	var diff_from_c = midi_note - midi_middle_c
	if diff_from_c < 0:
		return -1
	
	var line = max_line
	var step_delay = 0
	var next_step_delay = 2
	for i in diff_from_c:
		next_step_delay -= 1
		step_delay = next_step_delay
		if step_delay <= 0:
			line -= 1
			next_step_delay = midi_white_steps[(max_line-line) % midi_white_steps.size()]
	
	if step_delay != 0 or line < 1:
		return -1
	else:
		return line

func on_inventory_rest_toggled(enable_rest_mode):
	equipped_rest = enable_rest_mode
	if equipped_rest:
		cursor_node.texture = rest_textures[equipped_note_type]
	else:
		cursor_node.texture = note_textures[equipped_note_type]
