extends Node

onready var gates = get_children()
onready var adventurer = get_node("../Adventurer")

var queue_size = 0

func _process(delta):
	# Maybe change this so gates closing on-top of a character just kills them
	if queue_size > 0:
		for gate in gates:
			if gate.visibility_queued and not adventurer.is_on_gate(gate):
				gate.visible = true
				gate.visibility_queued = false
				queue_size -= 1

func show_all_gates():
	for gate in gates: 
		gate.visible = true
		gate.visibility_queued = false
	queue_size = 0

func on_playback_complete():
	show_all_gates()

func on_note_pressed(midi_note):
	for gate in gates:
		if midi_note == gate.note:
			gate.visible = false
			if gate.visibility_queued:
				gate.visibility_queued = false
				queue_size -= 1

func on_note_released(midi_note):
	for gate in gates:
		if midi_note == gate.note:
			if adventurer.is_on_gate(gate):
				gate.visibility_queued = true
				queue_size += 1
			else:
				gate.visible = true
				if gate.visibility_queued:
					gate.visibility_queued = false
					queue_size -= 1
