extends Sprite

signal on_click(midi_note)

export (int) var note = 60
export (String) var hover_text = ""
onready var label = $Label
var visibility_queued = false
var hovered = false

func _ready():
	label.text = hover_text
	label.visible = false

func _on_Area2D_mouse_entered():
	label.visible = true
	hovered = true

func _on_Area2D_mouse_exited():
	label.visible = false
	hovered = false

func _input(event):
	pass;

