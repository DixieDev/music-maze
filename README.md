# Music Maze

Written in Godot, and coincidentally started around the exact same time as the
[itch.io Rhythm Game Jam](https://itch.io/jam/rhythm-jam-2021), Music Maze has 
players compose music to manipulate a maze and help guide an adventurer into 
its mysterious depths.

Credits/attributions can be found within the attributions folder.